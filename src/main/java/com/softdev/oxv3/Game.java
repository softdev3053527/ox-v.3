/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.oxv3;

import java.util.Scanner;

/**
 *
 * @author slmfr
 */
public class Game {
    private Player player1,player2;
    private Board board;
    
    public Game(){
        player1 = new Player('X');
        player2 = new Player('O');
    }
    public void play(){
        boolean isFinish = false;
        newGame();
        while(!isFinish){
            printWelcom();
            printBoard();
            printTrun();
            inputRowCol();
            if(board.checkWin()){
                printBoard();
                showWinner();
                printPlayer();
                isFinish = true;
            }
            if(board.checkDraw()){
                printBoard();
                showDraw();
                printPlayer();
                isFinish = true;
            }
            board.switchPlayer();
        }
    }

    private void printWelcom() {
        System.out.println("Welcome to XO Game");
    }

    private void printBoard() {
        char[][] t = board.getBoard();
        for(int row = 0; row < 3 ;row++){
            System.out.println(" -------------");
            for(int col = 0 ; col < 3 ;col++){
                if(col%1 ==  0){
                    System.out.print(" | ");
                }
                System.out.print(t[row][col]);
            }
            System.out.println(" | ");
        }
        System.out.println(" -------------");
    }

    private void printTrun() {
        System.out.println("Turn " + board.getCurrentPlayer().getSymbol() + " move");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("{Please input row col :");
        int row = sc.nextInt();
        int col = sc.nextInt();
        board.setRowCol(row, col);
    }

    private void newGame() {
        board = new Board(player1,player2);
    }

    private void showWinner() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " Win");
    }

    private void showDraw() {
        System.out.println("Draw");
    }
    
    private void printPlayer(){
        System.out.println(player1);
        System.out.println(player2);

    }
}
