/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.oxv3;

/**
 *
 * @author slmfr
 */
public class Board {
   private Player player1,player2,currebtPlayer;
   private char[][] board = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
   private int row,col,count;
   
   public Board(Player player1,Player player2){
       this.player1 = player1;
       this.player2 = player2;
       this.currebtPlayer = player1;
   }
   
   public char[][] getBoard(){
       return board;
   }
   
   public Player getCurrentPlayer(){
       return currebtPlayer;
   }
   
   public boolean setRowCol(int row,int col){
       if(board[row-1][col-1] == '-'){
           board[row-1][col-1] = currebtPlayer.getSymbol();
           this.row = row-1;
           this.col = col-1;
           this.count++;
           return true;
           
       }
       return false;
   }
   public boolean checkWin(){
       if(checkRow()){
           saveWin();
           return true;
       }
       if(checkCol()){
           saveWin();
           return true;
       }
       if(checkX1()){
           saveWin();
           return true;
       }
       if(checkX2()){
           saveWin();
           return true;
       }
       return false;
   }
   public boolean checkDraw(){
       if(count == 9){
           player1.countDraw();
           player2.countDraw();
           return true;
       }
       return false;
   }
   
   public boolean checkRow(){
       return board[row][0] == currebtPlayer.getSymbol() && board[row][1] == currebtPlayer.getSymbol() && board[row][2] == currebtPlayer.getSymbol();
   }
   public boolean checkCol(){
       return board[0][col] == currebtPlayer.getSymbol() && board[1][col] == currebtPlayer.getSymbol() && board[2][col] == currebtPlayer.getSymbol();
   }
   public boolean checkX1(){
       return (board[0][0] == currebtPlayer.getSymbol() && board[1][1] == currebtPlayer.getSymbol() && board[2][2] == currebtPlayer.getSymbol());
   }
   public boolean checkX2(){
       return (board[0][2] == currebtPlayer.getSymbol() && board[1][1] == currebtPlayer.getSymbol() && board[2][0] == currebtPlayer.getSymbol());
   }
   private void saveWin(){
       if(currebtPlayer == player1){
           player1.countWin();
           player2.countLoss();
       }else{
           player1.countLoss();
           player2.countWin();
       }
   }

    void switchPlayer() {
        if(currebtPlayer == player1){
            currebtPlayer = player2;
        }else{
            currebtPlayer = player1;
        }
    }
}
