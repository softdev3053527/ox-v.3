/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.oxv3;

/**
 *
 * @author slmfr
 */
public class Player {
    private char symbol;
    private int loss;
    private int win;
    private int draw;
    
    public Player(char symbol){
        this.symbol = symbol;
        this.win = 0;
        this.loss = 0;
        this.draw = 0;
    }
    
    public char getSymbol(){
        return symbol;
    }
    
    public int getWin(){
        return win;
    }
    
    public int getLoss(){
        return loss;
    }
    public int getDraw(){
        return draw;
    }
    
    public void countWin(){
        win = win++;
    }
    
    public void countLoss(){
        loss = loss++;
    }
    
    public void countDraw(){
        draw = draw++;
    }
        
    public String toString(){
    
    return "Player "+ this.symbol + " Win = " + this.win; 
    }
    
}


